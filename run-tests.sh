#!/usr/bin/env sh

set -x
set -e

CONTAINER_NAME=$1
CONTAINER_TEST_IMAGE=$2

docker rm -f ${CONTAINER_NAME} || true

docker run --name ${CONTAINER_NAME} -id ${CONTAINER_TEST_IMAGE} ash

# tests
docker exec -t ${CONTAINER_NAME} ash -c 'id bender | grep "uid=2001(bender) gid=2001(bender) groups=2001(bender),2001(bender)"'
docker exec -t ${CONTAINER_NAME} ash -c 'test -f /opt/java/openjdk/bin/java' # file exists
docker exec -t ${CONTAINER_NAME} ash -c 'test -x /opt/java/openjdk/bin/java' # file is executable
docker exec -t ${CONTAINER_NAME} ash -c 'test -f /home/bender/docker-entrypoint.sh' # file exists
docker exec -t ${CONTAINER_NAME} ash -c 'test -x /home/bender/docker-entrypoint.sh' # file is executable

docker rm -f ${CONTAINER_NAME}

docker run --name ${CONTAINER_NAME} -e APP_WORK_DIR="/tmp" -t ${CONTAINER_TEST_IMAGE} ash -c 'pwd' | grep "/tmp"

docker rm -f ${CONTAINER_NAME}
