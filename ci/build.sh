#!/usr/bin/env sh

set -e

if [ -f ".env" ]; then
  source .env
fi

set -v

VERSION_FILE="version.txt"

VERSION=$( cat ${VERSION_FILE} )

./mvnw package --batch-mode -DskipTests -Dversion=${VERSION}

if [ -z "${CONTAINER_TEST_IMAGE}" ]; then
  CONTAINER_TEST_IMAGE=test-image
fi

docker build -t ${CONTAINER_TEST_IMAGE} target/
docker push ${CONTAINER_TEST_IMAGE}
